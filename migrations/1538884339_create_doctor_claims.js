const deployTargetAndDelegate = require('./support/deployTargetAndDelegate')
const DoctorClaims = artifacts.require("./DoctorClaims.sol")

module.exports = function(deployer, networkName) {
  return deployTargetAndDelegate(artifacts, deployer, DoctorClaims, networkName)
};
