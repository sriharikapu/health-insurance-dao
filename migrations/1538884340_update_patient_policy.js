const updateDeployedTargetContract = require('./support/updateDeployedTargetContract')

module.exports = function(deployer, networkName) {
  return updateDeployedTargetContract(deployer, artifacts, 'PatientPolicy')
};
