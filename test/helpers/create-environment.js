const toRegistryKey = require('../../migrations/support/toRegistryKey')

const envDeployWithDelegate = require('./env-deploy-with-delegate')

module.exports = async function createEnvironment(artifacts) {
  const DoctorManager = artifacts.require('./DoctorManager.sol')
  const Delegate = artifacts.require('./Delegate.sol')
  const Registry = artifacts.require("./Registry.sol")

  const registry = await Registry.new()

  // const caseInstance = await Case.new()
  // await registry.register(toRegistryKey('Case'), caseInstance.address)

  const doctorManager = await envDeployWithDelegate(registry, Delegate, DoctorManager, 'DoctorManager')

  return {
    registry,
    adminSettings,
    doctorManager
  }
}
