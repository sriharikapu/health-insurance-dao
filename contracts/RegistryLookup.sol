pragma solidity ^0.4.23;

import './DoctorClaims.sol';
import './DoctorManager.sol';
import './PatientPolicy.sol';
import './Registry.sol';

library RegistryLookup {

  function doctorClaims(Registry self) internal view returns (DoctorClaims) {
    return DoctorClaims(self.lookup(keccak256("DoctorClaims")));
  }

  function doctorManager(Registry self) internal view returns (DoctorManager) {
    return DoctorManager(self.lookup(keccak256("DoctorManager")));
  }

  function patientPolicy(Registry self) internal view returns (PatientPolicy) {
    return PatientPolicy(self.lookup(keccak256("PatientPolicy")));
  }

}
