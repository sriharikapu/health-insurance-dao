import doctorClaimsContractConfig from '#/DoctorClaims.json'
import doctorManagerContractConfig from '#/DoctorManager.json'
import patientPolicyContractConfig from '#/PatientPolicy.json'
import registryConfig from '#/Registry.json'

import { abiFactory } from '~/saga-genesis/utils'

export default {
  contractFactories: {
    DoctorClaims: abiFactory(doctorClaimsContractConfig.abi),
    DoctorManager: abiFactory(doctorManagerContractConfig.abi),
    PatientPolicy: abiFactory(patientPolicyContractConfig.abi),
    Registry: abiFactory(registryConfig.abi)
  }
}
