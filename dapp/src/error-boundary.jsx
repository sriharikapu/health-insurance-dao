import React from 'react'

let boundary

boundary = class ErrorBoundary extends React.Component {
  componentDidCatch(error, errorInfo) {
    // bugsnagClient.notify(error, errorInfo)
  }

  render() {
    return this.props.children;
  }
}

export const ErrorBoundary = boundary
