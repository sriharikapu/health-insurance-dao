import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import get from 'lodash.get'
import * as routes from '~/config/routes'
import { PageTitle } from '~/components/PageTitle'

function mapStateToProps(state) {
  const networkId = get(state, 'sagaGenesis.network.networkId')
  const address = get(state, 'sagaGenesis.accounts[0]')
  const signedIn = get(state, 'account.signedIn')

  return {
    address,
    signedIn
  }
}

export const Welcome = connect(mapStateToProps)(class _Welcome extends Component {
  render() {
    let launchLink

    if (this.props.isDoctor) {
      launchLink = routes.DOCTOR_CLAIMS_NEW
    } else {
      launchLink = routes.PATIENT_POLICIES_NEW
    }

    if (this.props.address === '0x5A9784CbfAe9DFC2A09a77a3C92a6a62156eaa12') {
      launchLink = routes.DOCTOR_CLAIMS_VERIFY
    }

    return (
      <div>
        <PageTitle renderTitle={(t) => t('pageTitles.welcome')} />
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12 col-md-10 col-md-offset-1'>
              <div className="card">
                <div className="card-body text-center">
                  <img src="/logo.svg" alt="logo" style={{ height: 75, width: '100%', margin: '60px 0 60px' }} />
                  <h2>
                    Welcome to OpenCoverage!
                  </h2>
                  <p className="lead text-center">To begin, download the MetaMask Extension (
                        <a target='_blank' rel="noopener noreferrer" href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en">Chrome</a>
                    &nbsp;/&nbsp;
                        <a target='_blank' rel="noopener noreferrer" href="https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/">Firefox</a>)
                        or the Coinbase Wallet mobile browser (<a target='_blank' rel="noopener noreferrer" href="https://itunes.apple.com/us/app/coinbase-wallet/id1278383455?mt=8">iOS</a>
                    &nbsp;/&nbsp;
                        <a target='_blank' rel="noopener noreferrer" href="https://play.google.com/store/apps/details?id=org.toshi&hl=en_CA">Android</a>).</p>

                  <br />
                  <br />
                </div>

                <div className="card-footer">
                  <div className='text-center'>
                    <Link
                      className="btn btn-lg btn-success"
                      to={launchLink}>
                      Launch OpenCoverage
                    </Link>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
})
