import React, { Component } from 'react'
import { EthAddress } from '~/components/EthAddress'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit';
import faCheck from '@fortawesome/fontawesome-free-solid/faCheck';
import sortBy from 'lodash.sortby'
import { isBlank } from '~/utils/isBlank'
import { withSend, withSaga, cacheCallValue, cacheCall, contractByName } from '~/saga-genesis'
import classnames from 'classnames'
import Select from 'react-select'
import * as Animated from 'react-select/lib/animated';
import { customStyles } from '~/config/react-select-custom-styles'
import { isNotEmptyString } from '~/utils/isNotEmptyString'
import { toastr } from '~/toastr'
import pull from 'lodash.pull'
import { all } from 'redux-saga/effects'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import get from 'lodash.get'
import range from 'lodash.range'

require('./RegisterDoctor.css')

const requiredFields = [
  'address',
  'name'
]

function mapStateToProps (state) {
  const DoctorManager = contractByName(state, 'DoctorManager')
  const doctorCount = cacheCallValue(state, DoctorManager, 'doctorCount')
  const doctors = []

  // doctorCount at 0 is empty records because Solidity, start at 1
  for (var i = 1; i < doctorCount; i++) {
    const address = cacheCallValue(state, DoctorManager, 'doctorAddresses', i)
    if (address) {
      doctors.push({
        doctorIndex: i,
        name: cacheCallValue(state, DoctorManager, 'doctorNames', i),
        address
      })
    }
  }

  return {
    DoctorManager,
    doctorCount,
    doctors
  }
}

function* saga({ DoctorManager }) {
  if (!DoctorManager) { return }

  const doctorCount = yield cacheCall(DoctorManager, 'doctorCount')

  // doctorCount at 0 is empty records because Solidity, start at 1
  const indices = range(1, doctorCount)

  yield all(
    indices.map(function* (i) {
      const address = yield cacheCall(DoctorManager, 'doctorAddresses', i)
      yield all([
        cacheCall(DoctorManager, 'doctorNames', i)
      ])
    })
  )
}

export const RegisterDoctorContainer =
  withSend(
    connect(mapStateToProps)(
      withSaga(saga)(
        class _RegisterDoctor extends Component {

          constructor(props){
            super(props)
            this.state = {
              address: '',
              name: '',
              errors: []
            }
          }

          handleSubmit = async (event) => {
            event.preventDefault()

            await this.runValidation()

            if (this.state.errors.length === 0) {
              this.addDoctor()
            }
          }

          addDoctor = () => {
            const { DoctorManager, send } = this.props
            send(
              DoctorManager,
              'addDoctor',
              this.state.address,
              this.state.name
            )()
          }

          validateField = (fieldName) => {
            if (!requiredFields.includes(fieldName)) {
              return
            }

            const errors = this.state.errors

            if (!isNotEmptyString(this.state[fieldName])) {
              errors.push(fieldName)
            } else {
              pull(errors, fieldName)
            }

            this.setState({ errors: errors })
          }

          runValidation = async () => {
            // reset error states
            await this.setState({ errors: [] })

            let errors = []
            let length = requiredFields.length

            for (var fieldIndex = 0; fieldIndex < length; fieldIndex++) {
              let fieldName = requiredFields[fieldIndex]
              if (!isNotEmptyString(this.state[fieldName])) {
                errors.push(fieldName)
              }
            }

            const existingDoc = this.props.doctors.find((doctor) => {
              return (
                doctor.address.toLowerCase() === this.state.address.toLowerCase()
              )
            })

            if (existingDoc) {
              errors.push('address')
              toastr.error('This address is already registered to another doctor.')
            }

            await this.setState({ errors: errors })

            if (errors.length > 0) {
              // First reset it so it will still take the user to the anchor even if
              // we already took them there before (still error on same field)
              window.location.hash = `#`;

              // Go to first error field
              window.location.hash = `#${errors[0]}`;
            }
          }

          errorMessage = (fieldName) => {
            return 'must be filled out'
          }

          handleButtonGroupOnChange = (event) => {
            this.setState({ [event.target.name]: event.target.value })
          }

          render() {
            const doctors = this.props.doctors

            let errors = {}
            for (var i = 0; i < this.state.errors.length; i++) {
              let fieldName = this.state.errors[i]

              errors[fieldName] =
                <p key={`errors-${i}`} className='has-error help-block small'>
                  {this.errorMessage(fieldName)}
                </p>
            }

            return (
              <div className='container'>
                <div className="row">
                  <div className="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div className="card">
                      <div className="card-header">
                        <h3 className="title">
                          Register a new Doctor
                        </h3>
                      </div>

                      <form onSubmit={this.handleSubmit}>
                        <div className="card-body">
                          <div className="form-wrapper">
                            <div className={classnames('form-group', { 'has-error': errors['address'] })}>
                              <label htmlFor="address">Address</label>
                              <input
                                id="address"
                                className="form-control"
                                value={this.state.address}
                                placeholder="0x000000000000000000000000000"
                                onChange={(e) => this.setState({address: e.target.value})}
                              />
                              {errors['address']}
                            </div>

                            <div className={classnames('form-group', { 'has-error': errors['name'] })}>
                              <label htmlFor="name">Name</label>
                              <input
                                className="form-control"
                                value={this.state.name}
                                placeholder='Dr. Hibbert'
                                onChange={(e) => this.setState({name: e.target.value})}
                                id="name"
                              />
                              {errors['name']}
                            </div>

                          </div>
                        </div>

                        <div className="card-footer text-right">
                          <button
                            type="submit"
                            className="btn btn-success btn-lg">
                            Register
                          </button>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-xs-12">
                    <div className="card">
                      <div className="card-body table-responsive">
                        <table className="table table-striped">
                          <thead>
                            <tr>
                              <th>Doctor Address</th>
                              <th>Doctor Details</th>
                            </tr>
                          </thead>
                          <tbody>
                            <TransitionGroup component={null}>
                              {doctors.map(({
                                address,
                                name,
                                doctorIndex
                              }) => {
                                return (
                                  <CSSTransition
                                    key={`doctor-row-transition-${doctorIndex}`}
                                    timeout={100}
                                    appear={true}
                                    classNames="fade">
                                      <tr key={`doctor-row-${doctorIndex}`}>
                                        <td width="46%" className="eth-address text">
                                          <span>
                                            <EthAddress address={address} showFull={true} />
                                          </span>
                                        </td>
                                        <td width="20%" className="td--status">
                                          {name}
                                        </td>
                                      </tr>
                                  </CSSTransition>
                                )
                              })}
                            </TransitionGroup>
                          </tbody>
                        </table>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            )
          }
        }
      )
    )
  )
