import React, { Component } from 'react'
import classnames from 'classnames'
import { all } from 'redux-saga/effects'
import { toastr } from '~/toastr'
import { isBlank } from '~/utils/isBlank'
import {
  Nav,
  Navbar,
  NavItem,
  NavDropdown,
  MenuItem
} from 'react-bootstrap'
import {
  IndexLinkContainer,
  LinkContainer
} from 'react-router-bootstrap'
import { withRouter, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import ReactTooltip from 'react-tooltip'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faEthereum from '@fortawesome/fontawesome-free-brands/faEthereum'
import faUserCircle from '@fortawesome/fontawesome-free-solid/faUserCircle'
import faWallet from '@fortawesome/fontawesome-free-solid/faWallet'
import faHeartbeat from '@fortawesome/fontawesome-free-solid/faHeartbeat';
import faUserMd from '@fortawesome/fontawesome-free-solid/faUserMd'
import faUser from '@fortawesome/fontawesome-free-solid/faUser'
import faCog from '@fortawesome/fontawesome-free-solid/faCog'
import { weiToEther } from '~/utils/weiToEther'
import get from 'lodash.get'
import { connect } from 'react-redux'
import {
  cacheCall,
  withSaga,
  cacheCallValue,
  contractByName
} from '~/saga-genesis'
import { CurrentTransactionsList } from '~/components/CurrentTransactionsList'
import * as routes from '~/config/routes'

function mapStateToProps (state) {
  let doctorName
  const address = get(state, 'sagaGenesis.accounts[0]')
  const DoctorManager = contractByName(state, 'DoctorManager')
  const isDoctor = cacheCallValue(state, DoctorManager, 'isDoctor')
  const isAdmin = cacheCallValue(state, DoctorManager, 'owner') === address
  let balance = get(state, 'sagaGenesis.ethBalance.balance')
  if (balance) {
    balance = weiToEther(balance)
  }
  const networkId = get(state, 'sagaGenesis.network.networkId')


  if (isDoctor) {
    doctorName = cacheCallValue(state, DoctorManager, 'name', address)
  }

  return {
    address,
    balance,
    doctorName,
    isDoctor,
    networkId,
    DoctorManager,
    isAdmin
  }
}

function mapDispatchToProps (dispatch) {
  return {
    dispatchSignOut: () => {
      dispatch({ type: 'SIGN_OUT' })
    }
  }
}

function* saga({ address, DoctorManager }) {
  if (!address || !DoctorManager) { return }
  yield all([
    // cacheCall(WrappedEther, 'balanceOf', address),
    cacheCall(DoctorManager, 'owner'),
    cacheCall(DoctorManager, 'isDoctor', address),
    cacheCall(DoctorManager, 'name', address)
  ])
}

export const OpenCoverageNavbar = connect(mapStateToProps, mapDispatchToProps)(
    withSaga(saga)(
      class _OpenCoverageNavbar extends Component {

  constructor(props) {
    super(props)

    this.state = {
      profileMenuOpen: false,
      adminMenuOpen: false
    }
  }

  signOut = () => {
    this.props.dispatchSignOut()
    this.props.history.push(routes.SIGN_IN)
  }

  toggleProfileMenu = (value) => {
    this.setState({ profileMenuOpen: value  })
  }

  render() {
    const { isDoctor } = this.props
    const nameOrAccountString = this.props.doctorName ? this.props.doctorName : 'Account'

    if (this.props.address) {
      var profileMenu =
        <NavDropdown
          title={
            <span>
              <FontAwesomeIcon
                icon={faUser}
                size='sm' />
              &nbsp;&nbsp;
                {nameOrAccountString}
            </span>
          }
          id='account-dropdown'
          open={this.state.profileMenuOpen}
          onToggle={(value) => this.toggleProfileMenu(value)}

        >
          <MenuItem header>
            Profile
          </MenuItem>

          <LinkContainer to={routes.ACCOUNT_WALLET}>
            <MenuItem href={routes.ACCOUNT_WALLET}>
              Wallet
            </MenuItem>
          </LinkContainer>

          <MenuItem divider />
          <MenuItem onClick={this.signOut}>
            Sign Out
          </MenuItem>
        </NavDropdown>

      var etherBalance = <LinkContainer to='#'>
        <MenuItem href='#'>
          <FontAwesomeIcon
            icon={faEthereum}
            data-tip='Profile' /> {this.props.balance}
        </MenuItem>
      </LinkContainer>


      var policies =
        <IndexLinkContainer to={routes.PATIENT_POLICIES_NEW}>
          <NavItem href={routes.PATIENT_POLICIES_NEW}>
            Policies
          </NavItem>
        </IndexLinkContainer>

      if (isDoctor) {
        var claims =
          <LinkContainer to={routes.DOCTOR_CLAIMS}>
            <NavItem href={routes.DOCTOR_CLAIMS}>
              <FontAwesomeIcon
                icon={faUserMd}
                size='sm' />
                &nbsp;
              Claims
            </NavItem>
          </LinkContainer>
      }

      if (this.props.isAdmin) {
        var adminItem =
          <NavDropdown
            title={
              <span>
                <FontAwesomeIcon
                  icon={faCog}
                  size='sm' />
                &nbsp;
                Admin
              </span>
            }
            id='admin-dropdown'
            open={this.state.adminMenuOpen}
            onToggle={(value) => this.setState({adminMenuOpen: !this.state.adminMenuOpen})}
          >
            <LinkContainer to={routes.ADMIN_DOCTORS}>
              <MenuItem href={routes.ADMIN_DOCTORS}>
                Doctors
              </MenuItem>
            </LinkContainer>
          </NavDropdown>
      }
    }

    let navbarClassName = classnames(
      'navbar',
      'container--nav',
      'navbar-default',
      'navbar-absolute'
    )

    return (
      <Navbar
        fixedTop
        collapseOnSelect
        id="mainNav"
        className={navbarClassName}>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={routes.HOME}>
              <img src="/logo.svg" alt="logo" style={{ height: 30 }} />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Nav className="nav--network-name">
          {this.props.isAdmin ? (
            <NavItem>
              <FontAwesomeIcon
                icon={faUserCircle}
                size='sm'
                className='text-gold'
                data-tip='This Ethereum account is a Contract Owner' />
              <ReactTooltip effect='solid' place='bottom' />
            </NavItem>
          ) : null}
        </Nav>
        <Navbar.Collapse>
          <Nav pullRight>
            <CurrentTransactionsList />
            {etherBalance}
            {policies}
            {claims}
            {adminItem}
            {profileMenu}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}))

export const OpenCoverageNavbarContainer = withRouter(OpenCoverageNavbar)
