import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PageTitle } from '~/components/PageTitle'
import { ScrollToTop } from '~/components/ScrollToTop'
import {
  withSaga,
  addContract,
  contractByName
} from '~/saga-genesis'
import get from 'lodash.get'
import { fixAddress } from '~/utils/fixAddress'

function mapStateToProps(state, { match }) {
  const networkId = get(state, 'sagaGenesis.network.networkId')
  const patientPolicyAddress = fixAddress(match.params.patientPolicyAddress)

  const PatientPolicyManager = contractByName(state, 'PatientPolicyManager')

  return {
    PatientPolicyManager,
    patientPolicyAddress,
    networkId
  }
}

function* saga({ PatientPolicyManager, match, networkId, fromBlock }) {
  if (!networkId) { return }

  const patientPolicyAddress = fixAddress(match.params.patientPolicyAddress)
}

export const PatientCaseContainer = connect(mapStateToProps)(
  withSaga(saga)(
    class _PatientCase extends Component {
      constructor (props) {
        super(props)
        this.state = {}
      }

      render() {
        const patientPolicyAddress = fixAddress(this.props.match.params.patientPolicyAddress)

        return (
          <div>
            <ScrollToTop />
            <PageTitle renderTitle={(t) => t('pageTitles.patientCase', { patientPolicyId: ('' + patientPolicyAddress).substring(0, 10) + ' ...'})} />
            <div className='container'>
              <div className="row">
                <div className='col-xs-12'>
                  insert policy status here
                </div>
              </div>
            </div>
          </div>
        )
      }
    }
  )
)
