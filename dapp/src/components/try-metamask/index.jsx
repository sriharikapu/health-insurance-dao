import React, { Component } from 'react'
import DownloadMetamaskButtonImg from '~/assets/img/button--download-metamask.png'
import AppStoreButtonImg from '~/assets/img/button--app-store.png'
import PlayStoreButtonImg from '~/assets/img/button--play-store.png'
import { PageTitle } from '~/components/PageTitle'

export const TryMetamask = class _TryMetamask extends Component {
  render () {
    return (
      <div>
        <PageTitle renderTitle={(t) => t('pageTitles.tryMetaMask')} />
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
              <h3 className='text-white text-center title--inverse'>
                To use OpenCoverage you will need to use an Ethereum wallet
              </h3>

              <div className="form-wrapper form-wrapper--account">
                <div className="form-wrapper--body form-wrapper--body__extra-padding text-center">
                  <h4>
                    <a href='https://metamask.io/' title='MetaMask' target="_blank" rel="noopener noreferrer">MetaMask</a> is a wallet extension for Chrome, Firefox and Brave browsers:
                  </h4>
                  <br />
                  <a href="https://metamask.io" title="Download Metamask" target="_blank" rel="noopener noreferrer"><img src={DownloadMetamaskButtonImg} alt="Metamask Download Button" width="200" /></a>
                  <br />
                  <br />
                  <hr />
                  <br />
                  <h4>
                    On mobile? Try the Coinbase Wallet browser:
                  </h4>
                  <a
                    href="https://itunes.apple.com/us/app/coinbase-wallet/id1278383455?mt=8"
                    target="_blank"
                    rel="noopener noreferrer"
                    title="Download Coinbase Wallet from Apple App Store">
                    <img src={AppStoreButtonImg} alt="Apple App Store Button" width="100" />
                  </a>
                  &nbsp; &nbsp; &nbsp;
                  <a
                    href="https://play.google.com/store/apps/details?id=org.toshi&hl=en_CA"
                    target="_blank"
                    rel="noopener noreferrer"
                    title="Download Coinbase Wallet from Google Play Store">
                    <img src={PlayStoreButtonImg} alt="Google Play Store Button" width="100" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
