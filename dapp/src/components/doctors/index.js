import ReactDOMServer from 'react-dom/server'
import React, { Component } from 'react'
import ReactTimeout from 'react-timeout'
import Select from 'react-select'
import { connect } from 'react-redux'
import * as Animated from 'react-select/lib/animated';
import { withRouter } from 'react-router-dom'
import { Checkbox } from 'react-bootstrap'
import FlipMove from 'react-flip-move'
import PropTypes from 'prop-types'
import { isBlank } from '~/utils/isBlank'
import { Link } from 'react-router-dom'
import {
    contractByName,
    TransactionStateHandler,
    withSend
} from '~/saga-genesis'
import { customStyles } from '~/config/react-select-custom-styles'
import { HEXStringDisplay } from '~/components/HEXStringDisplay'
import { HEXTextArea } from '~/components/forms/HEXTextArea'
import { Loading } from '~/components/Loading'
import { toastr } from '~/toastr'
import pull from 'lodash.pull'
import * as routes from '~/config/routes'
import {
    LinkContainer
} from 'react-router-bootstrap'
import { Button } from 'react-bootstrap'

function mapStateToProps(state, ownProps) {
    const ClaimsManager = contractByName(state, 'ClaimsManager')

    return {
        ClaimsManager,
        transactions: state.sagaGenesis.transactions
    }
}

export const DoctorsContainer = withRouter(ReactTimeout(connect(mapStateToProps)(withSend(class _VerifyClaimContainer extends Component {

    render() {


        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-xs-12 col-md-10 col-md-offset-1'>
                        <div className="card">
                            <div className="card-body">

                                <div style={{ float: 'right' }}>
                                    <LinkContainer to={routes.DOCTOR_CLAIMS_NEW}>
                                        <Button className="btn-primary">New Claim</Button>
                                    </LinkContainer>
                                </div>

                                <h1>Claims To Review</h1>
                                <ul>
                                    <li><Link to={routes.DOCTOR_CLAIMS_VERIFY}>Claim 1</Link></li>
                                    <li>Claim 2</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}))))
