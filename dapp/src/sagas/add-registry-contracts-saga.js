import {
  select,
  all
} from 'redux-saga/effects'
import {
  contractByName,
  addContract,
  web3Call
} from '~/saga-genesis'
const debug = require('debug')('addRegistryContractsSaga.js')

function* lookupAndAddContract(web3, name) {
  const Registry = yield select(contractByName, 'Registry')
  const address = yield web3Call(Registry, 'lookup', web3.utils.sha3(name))
  if (address) {
    yield addContract({address, name, contractKey: name})
    debug(`Added ${name} at ${address}`)
  } else {
    debug(`Skipping ${name} which is unregistered`)
  }
}

export default function* ({ web3 }) {
  yield all([
    lookupAndAddContract(web3, 'DoctorClaims'),
    lookupAndAddContract(web3, 'DoctorManager'),
    lookupAndAddContract(web3, 'PatientPolicy')
  ])
}
