import { takeEvery } from 'redux-saga/effects'
import { transactionErrorToCode } from '~/services/transactionErrorToCode'

function logError({ transactionId, error, call }) {
  const errorCode = transactionErrorToCode(error)

  if (errorCode !== 'userRevert') {
    console.error('TransactionError', `${call.toString()}: ${errorCode}: ${error}`, transactionId, error)
    // bugsnagClient.notify(
    //   {
    //     name: `TransactionError`,
    //     message: `${call.toString()}: ${errorCode}: ${error}`,
    //     transactionId,
    //     error
    //   }
    // )
  }
}

export function* failedTransactionListener() {
  yield takeEvery('TRANSACTION_ERROR', logError)
}
