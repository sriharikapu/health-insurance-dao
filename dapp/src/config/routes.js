// Signed-Out Routes
export const WELCOME = '/welcome'
export const LOGIN_METAMASK = '/login-metamask'
export const TRY_METAMASK = '/try-metamask'
export const HOME = '/'

export const ADMIN_DOCTORS = '/admin/doctors'

export const ACCOUNT_WALLET = '/account/wallet'

export const DOCTOR_CLAIMS = '/doctors/claims'
export const DOCTOR_CLAIMS_NEW = '/doctors/claims/new'
export const DOCTOR_CLAIMS_VERIFY = '/doctors/claims/verify'

export const PATIENT_POLICIES_NEW = '/patients/policies/new'
export const PATIENT_POLICIES = '/patients/policies'
export const PATIENT_POLICY = '/patients/policies/:policyAddress'

export const signedOutRoutes = [
  WELCOME, LOGIN_METAMASK, TRY_METAMASK, HOME
]
