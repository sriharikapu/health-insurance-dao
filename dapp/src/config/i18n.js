import i18next from 'i18next';

i18next
  .init({
    interpolation: {
      escapeValue: false
    },
    lng: 'en',
    ns: 'translation',
    resources: {
      en: {
        translation: {
          transactions: {
            blankState: 'Currently there are no pending transactions',
            publicKeys: 'Retrieving Account',
            setPublicKey: 'Registering Account',
            sendEther: 'Sending 1 ETH',
            sendDai: 'Sending 1000 DAI',
            withdraw: 'Withdrawing W-ETH',
            setBaseCaseFeeUsdWei: 'Updating case fee',
            approve: 'Approving DAI Spend',
            addPatientPolicy: 'Creating Policy'
          },
          transactionErrors: {
            userRevert: 'You rejected the transaction.',
            outOfGas: 'The transaction ran out of gas.',
            evmRevert: 'There was an error in the contract.',
            incorrectNonce: 'The nonce was incorrect (reset the account in MetaMask).',
            cancelled: 'The transaction was cancelled.',
            noContractFound: 'No contract found for address, you may be on the wrong network (MainNet vs. Ropsten).',
            nonceTooLow: 'The nonce was too low (possibly need to reset the account in MetaMask).',
            exceedsBlockGasLimit: 'The transaction gasLimit exceeds block gas limit.',
            replacementTransactionUnderpriced: 'The replacement transaction is underpriced (need to provide a higher gas limit).'
          },
          pageTitles: {
            welcome: 'Welcome | OpenCoverage',
            loginToMetaMask: 'Login to MetaMask | OpenCoverage',
            tryMetaMask: 'Try MetaMask | OpenCoverage',
            emergencyKit: 'Emergency Kit | OpenCoverage',
            changePassword: 'Change Password | OpenCoverage',
            mint: 'Mint | OpenCoverage',
            balance: 'Balance | OpenCoverage',
            signIn: 'Sign In | OpenCoverage',
            signUp: 'Sign Up | OpenCoverage',
            fourOhFour: '404 | OpenCoverage',
            adminSettings: 'Settings | OpenCoverage',
            adminFees: 'Fees | OpenCoverage',
            adminCases: 'Cases | OpenCoverage',
            updateAdminSettings: 'Updating Admin Settings'
          }
        }
      }
    }
  });

export default i18next;
